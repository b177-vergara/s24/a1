// 1

	db.users.find(
		{
			$or: [
			{ firstName: { $regex: 's', $options: '$i'}},
			{ lastName: { $regex: 'd', $options: '$i'}}
			]
		},
		{ firstName: 1, lastName: 1, _id: 0}
	)

// 2
	
	db.users.find(
		{
			$and: [
			{ age: {$gte: 70} },
			{ department: 'HR'}
			]
		}
	)

// 3

	db.users.find(
		{
			$and: [
			{ age: {$lte: 30} },
			{ firstName: { $regex: 'e', $options: '$i'}}
			]
		}
	)
